const path = require("path");
const http = require("http");
const uu_id = require("uuid");
const fs = require("fs");

const PORT = 3000;

// Creating server using `http` module
const server = http.createServer((req, res) => {
    const url = path.normalize(req.url);

    switch (url.split("/")[1]) {
        case "html": {
            fs.readFile(path.join(__dirname, "random.html"), "utf8", (err, data) => {
                if (err) {
                    res.writeHead(500);
                    res.end("<h1>Internal Server Error</h1>");
                } else {
                    res.writeHead(200, {"content-type": "text/html"});
                    res.end(data);
                }
            })
            break;
        }
        case "json": {
            fs.readFile(path.join(__dirname, "random.json"), "utf8", (err, data) => {
                if (err) {
                    res.writeHead(500);
                    res.end("<h1>Internal Server Error</h1>");
                } else {
                    res.writeHead(200, { "content-type": "application/json" });
                    res.end(data, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("Successfully returned JSON data.");
                        }
                    });
                }
            })
            break;
        }
        case "uuid": {
            const uuid = uu_id.v4();

            res.writeHead(200, { "content-type": "application/json" });
            res.end(JSON.stringify({ uuid }));
            break;
        }
        case "status_code": {
            if (http.STATUS_CODES[url.split("/")[2]]) {
                res.writeHead(url.split("/")[2]);
            } else {
                res.writeHead(404);
                res.end(`<h1>Not Found</h1>`);
            }

            res.end();
            break;
        }
        case "delay": {
            if (url.split("/")[2] == "") {
                setTimeout(() => {
                    res.writeHead(200);
                    res.end(`<h1>Hello, world!</h1>`);
                }, 0);
            } else if (!isNaN(url.split("/")[2])) {
                setTimeout(() => {
                    res.writeHead(200);
                    res.end(`<h1>Hello, world!</h1>`);
                }, parseInt(url.split("/")[2]) * 1000);
            } else {
                res.writeHead(404);
                res.end(`<h1>Not Found</h1>`);
            }
            break;
        }
        default: {
            res.writeHead(404);
            res.end(`<h1>Page not found!</h1>`);
        }
    }
})

//Listening on server
server.listen(PORT, (err) => {
    console.log("Listening on port: ", PORT);
})